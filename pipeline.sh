#!/bin/bash

if [[ "${#@}" -eq 0 ]]
then
	echo "No commands specified" >&2
	exit
fi

if [[ ${#@} -eq 1 ]]
then
	exec "$1"
else
	exec "${1}" | "$0" "${@:2}"
fi
