#!/usr/bin/lua

local json  = require("json")

s = io.read("*a")

local j=json.decode(s)

local tpl=assert(io.open(os.getenv("GROFF_TEMPLATE"), "r"))

function getPositions()
	local str="===\nlrxn\n.\n"
	local str2=".TS\n"
	local total=0
	for _,a in pairs(j["articles"]) do
		local escname=a["name"]
		escname=string.gsub(escname,"\\","\\\\")
		escname=string.gsub(escname,"\n","\n\\&")
		local subtot=a["price"]*a["amount"]
		total=total+subtot
		str=str..string.format("%d\tT{\n%s\nT}\t%.2f CHF\n", a["amount"], escname, subtot)
		str2=str2.."llxn\n"
	end
	str=str..string.format("\n\tTOTAL\t%.2f CHF\n", total)
	return str2..str..".TE";
end

local fctbl={
	["sale_id"]=function()
		return j["sale_id"]
	end,
	["positions"]=function()
		return getPositions()
	end,
	["today"]=function()
		return os.date("%Y-%m-%d %H:%M")
	end,
}

function processLine(t)
	a,b,name= string.find(t, "{{([^}]+)}}")
	if name then
		io.write(string.sub(t,1,a-1))
		if fctbl[name] then
			io.write(fctbl[name]())
		else
			io.write("{{"..name.."}}")
		end
		processLine(string.sub(t,b+1))
	else
		io.write(t)
	end
end

while true do
	t=tpl:read("*line")
	if not t then
		break
	end
	processLine(t.."\n")
end

tpl:close()
