RabbitMQ POS UNIX Service for Line-Printers (RaPOUNSeL)
======================================================

This program takes JSON-Messages from an AMQP Queue, and prints them to an ASCII printer

Requirements
------------

* amqp-tools
* lua
* lua-json
* bash
* groff

Set-Up
------

```
	mkdir env
	echo "user" > env/RABBIT_USER
	echo "password" > env/RABBIT_PASSWORD
```

Then execute the run-script, hopefully using the daemontools.
Additionally, one might have to provide a ./amqp-consume program newer than 0.8.0, e.g. by sym-linking it

```
	ln -s /usr/bin/amqp-consume .
```

Also, check the `./run` script that the setuidgid program uses the proper unix account

Operation
---------

The `./daemon.sh` script is just a wrapper around `amqp-consume`
Whenever a message gets sent to the queue, the `./start_pipeline.sh` script is executed.
This script (and `./pipeline.sh`) execute all executable files inside ./pipeline/ in lexical
order, and connects them to each other via stdout←→stdin.

Pipeline
--------

The pipeline does the following:

1. The JSON is parsed and applied to a groff template (a neat typesetting system historically used for man-pages)
2. This groff-file is then converted from UTF8 to ASCII
3. It is fed into the groff processor, which outputs an ascii file but with escape sequences for a VT100-terminal
4. These ANSI-Sequences are removed with `ansifilter`
5. Empty lines at the beginning of the file are removed
6. The file is sent off to the printer
