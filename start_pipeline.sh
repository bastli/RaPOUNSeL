#!/bin/bash

cd pipeline
EXES=()
for file in *
do
	if [[ -x "$file" ]]
	then
		EXES+=("./$file")
	fi
done
exec ../pipeline.sh "${EXES[@]}"
